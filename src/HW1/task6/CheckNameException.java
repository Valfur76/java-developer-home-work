package HW1.task6;

public class CheckNameException extends Exception {
    @Override
    public String toString() {
        return "Name is not correct";
    }
}
