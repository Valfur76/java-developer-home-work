package HW1.task6;

public class CheckHeightException extends Exception {
    @Override
    public String toString() {
        return "Height is not correct";
    }
}
